import pandas as pd
import networkx as nx
import numpy as np
import time
import sys,os,select
import math
import itertools as it
import operator as op

import dask
import dask.dataframe as dd
from dask import threaded, multiprocessing

import logging as log
import random
import functools

number_of_process = 2

def ncr(n, r):
    r = min(r, n-r)
    if r == 0: return 1
    numer = functools.reduce(op.mul, range(n, n-r, -1))
    denom = functools.reduce(op.mul, range(1, r+1))
    return numer//denom

def all_measures(row, g):
    if(row['source'] == 1 and row['destination'] == 1):
        print(row)
        return pd.Series({'union_measure': 0, 'union_cc': 0,
                      'fr_measure': 0, 'fr_cc': 0,
                      'w_src_pairs': 0, 'w_src_p_cc': 0,
                      'w_dst_pairs' : 0, 'w_dst_p_cc' : 0, 'tension': 0})
    
    dst_nbrs = set(g.neighbors(row['destination']))
    cmn_nbrs = set([]) 
    src_wo_cmn = [] 

    for n in g[row['source']]:
        if n in dst_nbrs:
            cmn_nbrs.add(n)
        else:
            src_wo_cmn.append(n)

    dst_wo_cmn = dst_nbrs - cmn_nbrs

    '''Inter Neighbor Density'''
    no_of_cross_links = len(src_wo_cmn) * len(dst_wo_cmn)
    count = 0
    for node in src_wo_cmn:
        count = count + len(set(g[node]).intersection(dst_wo_cmn))

    union_cc = 0
    union_measure = 0

    if (no_of_cross_links > 0):
        union_measure = count 
        union_cc = float(union_measure) / no_of_cross_links

    '''common neighbor measures'''
    fr_measure = 0
    fr_cc = 0

    if (len(cmn_nbrs) > 1):
        cmn = set(cmn_nbrs)
        while cmn:
            node = cmn.pop()
            fr_measure = fr_measure + len(set(g[node]).intersection(cmn))
        fr_cc = float(fr_measure) / ncr(len(cmn_nbrs), 2) 
    
    '''Intra Neighbor Density @ src end'''
    wsp_measure = 0
    wsp_cc = 0
    if (len(src_wo_cmn) > 1):
        srcn = set(src_wo_cmn)
        while srcn:
            node = srcn.pop()
            wsp_measure = wsp_measure + len(set(g[node]).intersection(srcn))
        wsp_cc = float(wsp_measure) / ncr(len(src_wo_cmn), 2)

    '''Intra Neighbor Density @ dst end'''
    wdp_measure = 0
    wdp_cc = 0
    if(len(dst_wo_cmn)>1):
        dstn = set(dst_wo_cmn)
        while dstn:
            node = dstn.pop()
            wdp_measure = wdp_measure + len(set(g[node]).intersection(dstn))
        wdp_cc = float(wdp_measure) / ncr(len(dst_wo_cmn), 2) 
    
    ten = 0.5*(union_cc + fr_cc - wsp_cc - wdp_cc)
    return pd.Series({'union_measure': union_measure, 'union_cc': union_cc, 
                      'fr_measure': fr_measure, 'fr_cc': fr_cc,
                      'w_src_pairs': wsp_measure, 'w_src_p_cc': wsp_cc,
                      'w_dst_pairs': wdp_measure, 'w_dst_p_cc' : wdp_cc,
                      'tension': ten})
 
def calculate_tension(g, feature_table):
    ddf = dd.from_pandas(feature_table, npartitions=number_of_process, sort=False)
    xyz = ddf.apply(all_measures, args=(g,), axis=1).compute(get=multiprocessing.get)
    ft_with_results = pd.concat([feature_table, xyz], axis=1)
    return ft_with_results

def ra_measures(row, g):
    if(row['source'] == 1 and row['destination'] == 1):
        print(row)
        return pd.Series({'resource_allocation': 0})
    
    return pd.Series({'resource_allocation': sum(1 / g.degree(w) for w in nx.common_neighbors(g, row['source'], row['destination']))})
   
def calculate_ra(g, feature_table):
    ddf = dd.from_pandas(feature_table, npartitions=number_of_process, sort=False)
    xyz = ddf.apply(ra_measures, args=(g,), axis=1).compute(get=multiprocessing.get)
    ft_with_results = pd.concat([feature_table, xyz], axis=1)
    return ft_with_results

def ad_measures(row, g):
    if(row['source'] == 1 and row['destination'] == 1):
        print(row)
        return pd.Series({'adamic_adar': 0})

    return pd.Series({'adamic_adar': sum(1 / math.log(g.degree(w)) for w in nx.common_neighbors(g, row['source'], row['destination']))})
   
def calculate_ad(g, feature_table):
    ddf = dd.from_pandas(feature_table, npartitions=number_of_process, sort=False)
    xyz = ddf.apply(ad_measures, args=(g,), axis=1).compute(get=multiprocessing.get)
    ft_with_results = pd.concat([feature_table, xyz], axis=1)
    return ft_with_results

def jc_measures(row, g):
    if(row['source'] == 1 and row['destination'] == 1):
        print(row)
        return pd.Series({'jaccard_coefficient': 0})
    
    u = row['source']
    v = row['destination']
    cnbors = list(nx.common_neighbors(g, u, v))
    union_size = len(set(g[u]) | set(g[v]))
    
    return pd.Series({'jaccard_coefficient': 0 if union_size == 0 else len(cnbors) / union_size})
   
def calculate_jc(g, feature_table):
    ddf = dd.from_pandas(feature_table, npartitions=number_of_process, sort=False)
    xyz = ddf.apply(jc_measures, args=(g,), axis=1).compute(get=multiprocessing.get)
    ft_with_results = pd.concat([feature_table, xyz], axis=1)
    return ft_with_results

def pa_measures(row, g):
    if(row['source'] == 1 and row['destination'] == 1):
        print(row)
        return pd.Series({'preferential_attachment': 0})
    
    u = row['source']
    v = row['destination']
    
    return pd.Series({'preferential_attachment': g.degree(u) * g.degree(v)})
   
def calculate_pa(g, feature_table):
    ddf = dd.from_pandas(feature_table, npartitions=number_of_process, sort=False)
    xyz = ddf.apply(pa_measures, args=(g,), axis=1).compute(get=multiprocessing.get)
    ft_with_results = pd.concat([feature_table, xyz], axis=1)
    return ft_with_results

def link_prediction(g, test_links):
    log.info('starting tension calculation')
    feature_table = pd.DataFrame.from_records(test_links, columns=('source','destination'))
    tm = calculate_tension(g, feature_table)
    log.info('done')
    feature_table.to_csv("partial_data_after_tension.csv")
    
    log.info('starting resource allocation')
    ra = calculate_ra(g, feature_table)
    log.info('done')
    ra.to_csv("partial_data_ra.csv")

    log.info('starting adamin adar')
    ad = calculate_ad(g, feature_table)
    log.info('done')
    ad.to_csv("partial_data_ad.csv")

    log.info('starting jaccard coefficient') 
    jc = calculate_jc(g, feature_table)
    log.info('done')
    jc.to_csv("partial_data_jc.csv")

    log.info('starting preferential attachment')
    pa = calculate_pa(g, feature_table)
    log.info('done')
    pa.to_csv("partial_data_pa.csv")

    log.info('merging results')
    merged = pd.merge(pd.merge(pd.merge(ra, ad, on=['source', 'destination']), jc, on=['source', 'destination']), pa, on=['source', 'destination'])
    log.info('done')

    log.info('saving files')
    result = pd.merge(tm, merged, on=['source', 'destination'])
    log.info('done')
    return result

def tension_measures(g, test_links):
    log.info('starting tension calculation')
    feature_table = pd.DataFrame.from_records(test_links, columns=('source','destination'))
    tm = calculate_tension(g, feature_table)
    log.info('done')

    return tm
