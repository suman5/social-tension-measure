The codebase for paper "Double Bounded Rough Set, Tension Measure and Social Link Prediction

### Abstract 

The paper describes a new approach of viewing a social relation as a string with
various forces acting on it. Accordingly, a tension measure for a relation is 
defined. Various component forces of the tension measure are identified 
based on the structural information of the network. A new variant of rough set, 
namely, double bounded rough set is developed in order to define these forces 
mathematically. It is revealed experimentally with synthetic and real-world 
data that positive and negative tension characterize, relatively, the presence 
and absence of a physical link between two nodes. An algorithm based on tension 
measure is proposed for link prediction. Superiority of the algorithm is 
demonstrated on nine real-world networks which include four temporal networks.