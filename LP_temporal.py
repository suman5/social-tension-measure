import pandas as pd
import networkx as nx
import numpy as np
import time
import sys,os,select
import math
import itertools as it

import dask
import dask.dataframe as dd
from dask import threaded, multiprocessing

import logging as log
import random
import tension.tension as tn 

'''File names for log/data set/results'''
log.basicConfig(filename=str(sys.argv[3]), format='%(asctime)s %(message)s', level=log.DEBUG)

data = str(sys.argv[1])
result = str(sys.argv[2])
per = int(sys.argv[4])

print(data)
print(result)

df = pd.read_csv(data, sep=" ")
#df1 = df.sort_values(by='time')
#df2 = df1.reset_index(drop=True)

g = nx.from_pandas_dataframe(df, 'src', 'dst', 'time')

time = nx.get_edge_attributes(g, 'time')
existing_edges_sorted = sorted(time, key=time.get)

#existing_edges = g.edges()

upto = int(len(existing_edges_sorted)*per/100)

test_edges = []
test_non_edges = []

nodes = g.nodes()
while len(test_non_edges) < upto:
    x = random.sample(nodes, 100)
    y = random.sample(nodes, 100)

    for a,b in zip(x,y):
        if g.has_edge(a,b) == False:
            test_non_edges.append((a,b))

test_edges = existing_edges_sorted[:upto]

for remove in test_edges:
    g.remove_edge(*remove)

log.info('edged sample selected:' + str(len(test_edges)))
log.info('non edge sample selected:' + str(len(test_non_edges)))

com = tn.link_prediction(g, test_edges)
com.to_csv(result + ".csv")

com = tn.link_prediction(g, test_non_edges)
com.to_csv(result + ".nl.csv")
